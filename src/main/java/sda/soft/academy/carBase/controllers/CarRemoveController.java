package sda.soft.academy.carBase.controllers;

import sda.soft.academy.carBase.dto.CarDto;
import sda.soft.academy.carBase.services.CarService;
import sda.soft.academy.carBase.services.CarServiceInMemoryImlementation;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Calendar;

@WebServlet(name="carEditController", value="/car/remove")
public class CarRemoveController extends HttpServlet {

    CarService carService = new CarServiceInMemoryImlementation();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String id = req.getParameter("id");

        Long carId = Long.valueOf(id);

        CarDto carDtoToEdit = carService.findCar(carId);
        carService.removeCar(carDtoToEdit);

        resp.sendRedirect("/car/list");

    }
}
