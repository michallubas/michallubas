package sda.soft.academy.carBase.controllers;

import sda.soft.academy.carBase.dto.CarDto;
import sda.soft.academy.carBase.services.CarService;
import sda.soft.academy.carBase.services.CarServiceInMemoryImlementation;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Calendar;

@WebServlet(name="carEditController", value="/car")
public class CarEditController extends HttpServlet {

    CarService carService = new CarServiceInMemoryImlementation();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

    String id = req.getParameter("id");
    //I called servlet /car at car_jstl.jsp, transfered here id from car_jstl

    Long carId = Long.valueOf(id);

        CarDto carDtoToEdit = carService.findCar(carId);
        req.setAttribute("car",carDtoToEdit);

        RequestDispatcher requestDispatcher = req.getRequestDispatcher("/cars_jsp/car.jsp");
        requestDispatcher.forward(req,resp);
    // a potem lądujemy w serlecie car.jsp, i ten obiekt (ktory wcześniej przyleciał z car_jstl
    // jest przenoszony do car.jsp i wrzucany w formluarz

    }
}
