package sda.soft.academy.carBase.controllers;


import sda.soft.academy.carBase.dto.CarDto;
import sda.soft.academy.carBase.services.CarService;
import sda.soft.academy.carBase.services.CarServiceInMemoryImlementation;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet( name="carSaveController", value="/car/save")
public class CarSaveController extends HttpServlet {

    CarService carService = new CarServiceInMemoryImlementation();

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String model=req.getParameter("model");
        String title=req.getParameter("name");
        String id=req.getParameter("id");

        Long idLong=null;
        try {
            idLong = Long.valueOf(id);
        } catch (Exception e) {

        }


        CarDto carDto = new CarDto(idLong,model,title);
        carService.saveCar(carDto);

        resp.sendRedirect("/car/list");


    }
}
