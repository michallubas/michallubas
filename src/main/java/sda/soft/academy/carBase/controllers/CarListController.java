package sda.soft.academy.carBase.controllers;

import sda.soft.academy.carBase.dto.CarDto;
import sda.soft.academy.carBase.services.CarService;
import sda.soft.academy.carBase.services.CarServiceInMemoryImlementation;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet (name="carListController", value="/car/list")
public class CarListController extends HttpServlet {

    CarService carService = new CarServiceInMemoryImlementation();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        // MODEL

        List<CarDto> carList = carService.findCars();

        // Cars to model
        req.setAttribute("carsModel", carList);

        // sterring to jsp
        RequestDispatcher requestDispatcher = req.getRequestDispatcher("/cars_jsp/cars_jstl.jsp");

        requestDispatcher.forward(req,resp);

   }
}
