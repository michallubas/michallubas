package sda.soft.academy.carBase.services;


import sda.soft.academy.carBase.dto.CarDto;

import java.util.List;

public interface CarService {

    List<CarDto> findCars ();

    CarDto findCar (Long id);

    void saveCar (CarDto carDto);

    void removeCar( CarDto carDto);
}
