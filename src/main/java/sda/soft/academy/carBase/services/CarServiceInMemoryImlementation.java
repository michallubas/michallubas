package sda.soft.academy.carBase.services;

import sda.soft.academy.carBase.dto.CarDto;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CarServiceInMemoryImlementation  implements CarService {


    private static Map<Long, CarDto> cars= new HashMap<>();
    static Long id=3l;

    {
        cars.put(1l, new CarDto(1l,"Skoda","Octavia"));
        cars.put(2l, new CarDto(2l,"Opel","Corsa"));
        cars.put(3l, new CarDto(3l,"Audi","A4"));

    }

    @Override
    public void removeCar(CarDto carDto) {
        CarDto carDtoToRemove = findCar(carDto.getId());
        cars.remove(carDtoToRemove);
    }


    @Override
    public List<CarDto> findCars() {
        return new ArrayList<>(cars.values());
    }

    @Override
    public CarDto findCar(Long id) {
        CarDto searchedCar = cars.get(id);
        return searchedCar;
    }

    @Override
    public void saveCar(CarDto carDto) {

        if(carDto.getId()!=null) {
            CarDto carDtoToOverride = findCar(carDto.getId());
            carDtoToOverride.setModel(carDto.getModel());
            carDtoToOverride.setName(carDto.getName());
        } else {

            cars.put(++id, new CarDto(++id,carDto.getName(),carDto.getModel()));

        }


    }
}
