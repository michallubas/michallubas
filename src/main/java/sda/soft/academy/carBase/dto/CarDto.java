package sda.soft.academy.carBase.dto;

public class CarDto {

    private Long id;
    private String name;
    private String condition;
    private String model;
    private int productionYear;

    public CarDto(Long id, String name, String model) {
        this.id = id;
        this.name = name;

        this.model = model;

    }

    public CarDto() {
    }

    @Override
    public String toString() {
        return "CarDto{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", condition='" + condition + '\'' +
                ", model='" + model + '\'' +
                ", productionYear=" + productionYear +
                '}';
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getCondition() {
        return condition;
    }

    public String getModel() {
        return model;
    }

    public int getProductionYear() {
        return productionYear;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setModel(String model) {
        this.model = model;
    }
}
