package sda.soft.academy.carBase;

import sda.soft.academy.carBase.dto.CarDto;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class main {

    public static void main(String[] args) {

         Map<Long, CarDto> cars= new HashMap<>();

        {
            cars.put(1l, new CarDto(1l,"Skoda","Octavia"));
            cars.put(2l, new CarDto(2l,"Opel","Corsa"));
            cars.put(3l, new CarDto(3l,"Audi","A4"));

        }

        System.out.println(new ArrayList<CarDto>(cars.values()));
    }
}
