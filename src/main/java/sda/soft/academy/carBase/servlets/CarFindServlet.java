package sda.soft.academy.carBase.servlets;

import sda.soft.academy.carBase.dto.CarDto;
import sda.soft.academy.carBase.services.CarService;
import sda.soft.academy.carBase.services.CarServiceInMemoryImlementation;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name="carFindServlet",value="/editCar")
public class CarFindServlet extends HttpServlet {

    CarService carService = new CarServiceInMemoryImlementation();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        PrintWriter out = resp.getWriter();


        String id = req.getParameter("findCarWithid");

        CarDto car = carService.findCar(Long.parseLong(id));

        out.println(car.getName());


    }
}
