package sda.soft.academy.carBase.servlets;

import sda.soft.academy.carBase.dto.CarDto;
import sda.soft.academy.carBase.services.CarService;
import sda.soft.academy.carBase.services.CarServiceInMemoryImlementation;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;

@WebServlet(name="carsEditServlet", value="/editCars")
public class CarsEditServlet extends HttpServlet {

    CarService carService = new CarServiceInMemoryImlementation();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        PrintWriter out = resp.getWriter();

        ArrayList<CarDto> cars = (ArrayList<CarDto>) carService.findCars();

        out.println("<html>\n" +
                "<body>\n" +
                "\n" +
                "<h4>List of cars</h4>\n" +
                "<ul>\n");

        for (CarDto carDto: cars) {
            out.println("<li>"+carDto.getModel()+"</li>");
        }

        out.println("</ul>\n" +
                "\n" +
                "</body>\n" +
                "</html>\n");

     }
}
