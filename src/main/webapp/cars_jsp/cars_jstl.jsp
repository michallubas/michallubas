<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<html>
<head>
    <title>List of cars</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css">

    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

    <!-- Popper JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js"></script>

    <!-- Latest compiled JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js"></script>

    <meta name="viewport" content="width=device-width, initial-scale=1">

</head>

<body>
<jsp:include page="nav.jsp"/>
<div class="container-fluid">
    <table class="table">
        <thead>
        <!-- headlines -->
        <tr>
            <th>Title</th>
            <th>Body</th>
            <th></th>
            <th></th>
        </tr>
        </thead>
        <tbody>
        <c:forEach items="${carsModel}" var="car">
            <tr>
                <!-- inputs to columns's headlines -->
                <td>${car.name}</td>
                <td>${car.model}</td>
                <td><a href="/car?id=${car.id}">Edit</a> </td>
                <!-- i did forward to servler /car (CarEditController) -->
                <td><a href="/car/remove=${car.id}">Remove</a> </td>

            </tr>
        </c:forEach>
        </tbody>
    </table>
</div>
</body>
</html>
