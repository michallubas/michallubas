<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css">

    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

    <!-- Popper JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js"></script>

    <!-- Latest compiled JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js"></script>

    <meta name="viewport" content="width=device-width, initial-scale=1">

</head>
<body>

<h2>Car edit</h2>

<jsp:include page="nav.jsp"/>
<div class="container-fluid">

    <!-- co to robi->  <form action="/tweet/save" method="post">  -->
    <form action="/car/save" method="post">
        <!-- i'm sending to server -->
        <div class="form-group">
            <label for="model">Car title:</label>
            <input type="text" name="model" class="form-control" id="model" value="${car.model}">
        </div>
        <div class="form-group">
            <label for="name">Car name:</label>
            <textarea class="form-control" rows="5" id="name" name="name">${car.name}</textarea>
        </div>
        <input type="hidden" name="id" value="${car.id}">
        <br><br>
        <input type="submit" value="Submit">
    </form>
</div>
<p>If you click the "Submit" button, the form-data will be sent to a page called "/action_page.php".</p>

</body>
</html>